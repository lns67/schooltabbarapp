//
//  DropDownViewController+Extensions.swift
//  SchoolTabBar
//
//  Created by Nazar Lysak on 24.10.2021.
//

import Foundation
import UIKit

// MARK: - DropDownViewController + Extensions

// MARK: - UITableViewDelegate, UITableViewDataSource
extension DropDownViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return classesArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return classesArray[section].studentsList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: tableViewCellID, for: indexPath) as! ClassTableViewCell
        cell.update(text: "\(classesArray[indexPath.section].studentsList[indexPath.row].surname) \(classesArray[indexPath.section].studentsList[indexPath.row].name)")
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 40))
        let label = UILabel(frame: view.frame)
        label.text = classesArray[section].className
        label.textAlignment = .center
        label.textColor = .systemBlue
        label.font = UIFont.systemFont(ofSize: 30)
        view.addSubview(label)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if tableView == dropDownTableView {
//            let selectedData = classesArray[indexPath.row]
//            genderButton.setTitle(selectedData, for: .normal)
//            self.dropDownTableView.isHidden = true
//            let currentCell = dropDownTableView.cellForRow(at: indexPath)! as UITableViewCell
//            let finalresult = currentCell.textLabel!.text!
//            print("\(finalresult)")
//        } else {
//            let selectedBlood = bloodList[indexPath.row]
//            bloodButton.setTitle(selectedBlood, for: .normal)
//            self.bloodTable.isHidden = true
//            let currentCellBlood = bloodTable.cellForRow(at: indexPath)! as UITableViewCell
//            let finalresultBlood = currentCellBlood.textLabel!.text!
//            print("\(finalresultBlood)")
//        }
    }
}
