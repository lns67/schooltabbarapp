//
//  DropDownViewController.swift
//  SchoolTabBar
//
//  Created by Nazar Lysak on 24.10.2021.
//

import UIKit

// MARK: - DropDownViewController

class DropDownViewController: UIViewController {

    @IBOutlet weak var dropDownTableView: UITableView!
    
    var classesArray: [ClassModel] = []
    let tableViewCellID = "ClassTableViewCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dropDownTableView.register(UINib(nibName: tableViewCellID, bundle: nil), forCellReuseIdentifier: tableViewCellID)
        classesArray = SourceManager().setupClasses()
        
        dropDownTableView.reloadData()
    }
}
