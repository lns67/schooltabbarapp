//
//  StudentsViewController+Extensions.swift
//  SchoolTabBar
//
//  Created by Nazar Lysak on 24.10.2021.
//

import Foundation
import UIKit

// MARK: - StudentsViewController + Extensions

// MARK: - UITableViewDelegate, UITableViewDataSource
extension StudentsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return studentsArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: tableViewCellID, for: indexPath) as! ClassTableViewCell
        cell.update(text: "\(studentsArray[indexPath.row].surname) \(studentsArray[indexPath.row].name)")
        return cell
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
               
            let classesVC = (self.tabBarController?.viewControllers?[0] as? ClassesViewController)
            
            let graduatedVC = (self.tabBarController?.viewControllers?[3] as? GraduatedStudentViewController)
            graduatedVC?.graduatedStudentsArray.append(studentsArray[indexPath.row])
            
            if let index = self.index {
                classModel[index].studentsList.remove(at: indexPath.row)
                classesVC?.classesArray = classModel
            }
            
            studentsArray.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
}
