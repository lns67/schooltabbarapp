//
//  StudentsViewController.swift
//  SchoolTabBar
//
//  Created by Nazar Lysak on 24.10.2021.
//

import UIKit

// MARK: - StudentsViewController

class StudentsViewController: UIViewController {

    @IBOutlet weak var classLabel: UILabel!
    @IBOutlet weak var studentsTableView: UITableView!
    
    var studentsArray: [StudentModel] = []
    var classModel: [ClassModel] = []
    var index: Int?
    var classLabelText = ""
    let tableViewCellID = "ClassTableViewCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        studentsTableView.register(UINib(nibName: tableViewCellID, bundle: nil), forCellReuseIdentifier: tableViewCellID)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        classLabel.text = classLabelText
        
        studentsTableView.reloadData()
    }

}
