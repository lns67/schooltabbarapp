//
//  StudentModel.swift
//  SchoolTabBar
//
//  Created by Nazar Lysak on 24.10.2021.
//

import Foundation

// MARK: - StudentModel

struct StudentModel {
    var name: String
    var surname: String
}
