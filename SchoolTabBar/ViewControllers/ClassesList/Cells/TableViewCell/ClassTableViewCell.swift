//
//  ClassTableViewCell.swift
//  SchoolTabBar
//
//  Created by Nazar Lysak on 24.10.2021.
//

import UIKit

// MARK: - ClassTableViewCell

class ClassTableViewCell: UITableViewCell {

    @IBOutlet weak var infoLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func update(text: String) {
        infoLabel.text = text
    }
}
