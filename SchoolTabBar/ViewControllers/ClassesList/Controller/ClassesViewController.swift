//
//  ClassesViewController.swift
//  SchoolTabBar
//
//  Created by Nazar Lysak on 24.10.2021.
//

import UIKit

// MARK: - ClassesViewController

class ClassesViewController: UIViewController {

    @IBOutlet weak var classesTableView: UITableView!
    
    let tableViewCellID =  "ClassTableViewCell"
    var classesArray: [ClassModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        classesArray = SourceManager().setupClasses()
        
        classesTableView.register(UINib(nibName: tableViewCellID, bundle: nil), forCellReuseIdentifier: tableViewCellID)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        classesArray.sort(by: {$0.className < $1.className})
        
        classesTableView.reloadData()
    }

}
