//
//  ClassModel.swift
//  SchoolTabBar
//
//  Created by Nazar Lysak on 24.10.2021.
//

import Foundation

// MARK: - ClassModel

struct ClassModel {
    var className: String
    var studentsList: [StudentModel]
}
