//
//  ClassesViewController+Extensions.swift
//  SchoolTabBar
//
//  Created by Nazar Lysak on 24.10.2021.
//

import Foundation
import UIKit

// MARK: - ClassesViewController + Extensions

// MARK: - UITableViewDelegate, UITableViewDataSource
extension ClassesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return classesArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: tableViewCellID, for: indexPath) as! ClassTableViewCell
        cell.update(text: classesArray[indexPath.row].className)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let studentsVC = (self.tabBarController?.viewControllers?[1] as? StudentsViewController)
        studentsVC?.studentsArray = self.classesArray[indexPath.row].studentsList
        studentsVC?.classLabelText = "\(self.classesArray[indexPath.row].className) Class"
        studentsVC?.classModel = self.classesArray
        studentsVC?.index = indexPath.row
        self.tabBarController?.selectedIndex = 1
    }
}
