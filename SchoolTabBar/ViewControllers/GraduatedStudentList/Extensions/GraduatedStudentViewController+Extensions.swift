//
//  GraduatedStudentViewController+Extensions.swift
//  SchoolTabBar
//
//  Created by Nazar Lysak on 24.10.2021.
//

import Foundation
import UIKit

// MARK: - GraduatedStudentViewController + Extensions

// MARK: - UITableViewDelegate, UITableViewDataSource
extension GraduatedStudentViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return graduatedStudentsArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: tableViewCellID, for: indexPath) as! ClassTableViewCell
        cell.update(text: "\(graduatedStudentsArray[indexPath.row].surname) \(graduatedStudentsArray[indexPath.row].name)")
        return cell
    }
}
