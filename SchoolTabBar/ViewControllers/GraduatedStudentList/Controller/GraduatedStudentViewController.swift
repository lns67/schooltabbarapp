//
//  GraduatedStudentViewController.swift
//  SchoolTabBar
//
//  Created by Nazar Lysak on 24.10.2021.
//

import UIKit

// MARK: - GraduatedStudentViewController

class GraduatedStudentViewController: UIViewController {

    @IBOutlet weak var graduatedTableView: UITableView!
    
    var graduatedStudentsArray: [StudentModel] = []
    let tableViewCellID = "ClassTableViewCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        graduatedTableView.register(UINib(nibName: tableViewCellID, bundle: nil), forCellReuseIdentifier: tableViewCellID)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        graduatedTableView.reloadData()
    }
}
