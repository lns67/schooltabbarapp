//
//  AddStudentViewController.swift
//  SchoolTabBar
//
//  Created by Nazar Lysak on 24.10.2021.
//

import UIKit

// MARK: - AddStudentViewController

class AddStudentViewController: UIViewController {

    @IBOutlet weak var classNameTextField: UITextField!
    @IBOutlet weak var studentNameTextField: UITextField!
    @IBOutlet weak var studentSurnameTextField: UITextField!
    
    var classesArray: [ClassModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func addStudentAction(_ sender: Any) {
        guard let className = classNameTextField.text, let studentName = studentNameTextField.text, let studentSurname = studentSurnameTextField.text, !className.isEmpty, !studentName.isEmpty, !studentSurname.isEmpty else {
            let alertVC = UIAlertController(title: "Error", message: "Please, enter all fields!", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertVC.addAction(cancelAction)
            present(alertVC, animated: true, completion: nil)
            return
        }
        
        let addedStudent = StudentModel(name: studentName, surname: studentSurname)
        let addedClass = ClassModel(className: className, studentsList: [addedStudent])
        
        let classesVC = (self.tabBarController?.viewControllers?[0] as? ClassesViewController)
        self.classesArray = classesVC?.classesArray ?? []
        
        var classWhereToAdd = classesArray.filter({$0.className == addedClass.className})
        
        if classWhereToAdd.isEmpty {
            classesArray.append(addedClass)
        } else {
            classWhereToAdd[0].studentsList.append(addedStudent)
            
            self.classesArray.enumerated().forEach { (index, classElement) in
                if classElement.className == addedClass.className {
                    self.classesArray.remove(at: index)
                    self.classesArray.append(classWhereToAdd[0])
                }
            }
        }
        
        classesVC?.classesArray = self.classesArray
        
        self.tabBarController?.selectedIndex = 0
        
        classNameTextField.text = ""
        studentNameTextField.text = ""
        studentSurnameTextField.text = ""
    }
}
