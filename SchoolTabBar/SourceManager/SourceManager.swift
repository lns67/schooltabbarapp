//
//  SourceManager.swift
//  SchoolTabBar
//
//  Created by Nazar Lysak on 24.10.2021.
//

import Foundation

// MARK: - SourceManager

class SourceManager {
    
    func setupStudents() -> [StudentModel]{
        
        let student1 = StudentModel(name: "Nazar", surname: "Lysak")
        let student2 = StudentModel(name: "Roman", surname: "Lysak")
        let student3 = StudentModel(name: "Petro", surname: "Krub")
        let student4 = StudentModel(name: "Nazar", surname: "Yurchen")
        let student5 = StudentModel(name: "Ostap", surname: "Mamcher")
        let student6 = StudentModel(name: "Denys", surname: "Wernoch")
        let student7 = StudentModel(name: "Anna", surname: "Lobova")
        let student8 = StudentModel(name: "Julia", surname: "Zorniak")
        let student9 = StudentModel(name: "Ira", surname: "Petryshyn")
        let student10 = StudentModel(name: "Adam", surname: "Lysak")
        
        return [student1, student2, student3, student4, student5, student6, student7, student8, student9, student10]
    }
    
    func setupClasses() -> [ClassModel]{
        
        let class1 = ClassModel(className: "1-a", studentsList: setupStudents())
        let class2 = ClassModel(className: "2-a", studentsList: setupStudents())
        let class3 = ClassModel(className: "3-b", studentsList: setupStudents())
        let class4 = ClassModel(className: "4-a", studentsList: [])
        let class5 = ClassModel(className: "5-b", studentsList: setupStudents())
        let class6 = ClassModel(className: "6-a", studentsList: setupStudents())
        let class7 = ClassModel(className: "7-b", studentsList: setupStudents())
        let class8 = ClassModel(className: "8-a", studentsList: [])
        
        return [class1, class2, class3, class4, class5, class6, class7, class8]
    }
}
